import playerlib
import gamelib
import matplotlib.pyplot as plt
import time


def graph(x, y, xlabel, ylabel):
    plt.figure(figsize=(10, 10))
    plt.plot(x, y)
    plt.title("Joueur MonteCarlo")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()

def game(difficulty, nbrParty, scores, nbrSimulation, temps):
    VictoireNoir = 0
    VictoireBlanc = 0
    timeStart = 0

    for i in range(nbrParty):
        jeu = gamelib.GameClass(5, 0.5, {"choix_joueurs": False})

        timeStart = time.time()

        jeu.blanc = playerlib.Random(jeu, -1)
        jeu.noir = playerlib.MonteCarlo(jeu, 1, difficulty)
        jeu.joueur_courant = jeu.noir

        while(not jeu.partie_finie):
            jeu.jouer(jeu.joueur_courant.donne_coup(jeu))

        score =  jeu.score()
        difficulty += 1

        print("Partie :", i+1 )
        if (score < 0):
            VictoireBlanc += 1
            print("Blanc gagne")
        else:
            VictoireNoir += 1
            print("Noir gagne")

        scores.append(score)

        nbrSimulation.append(difficulty)
        temps.append(time.time() - timeStart)


    print("Black : ", VictoireNoir, "White :", VictoireBlanc)


    print("coups:", nbrSimulation)
    print("score:", scores)
    print("temps:", time)

    return difficulty



scores = []
nbrSimulation = []
temps = []
difficulty = 1
for i in range(5):
    print("Simulation ", i+1 )
    difficulty = game(difficulty, 5, scores, nbrSimulation, temps)


graph(nbrSimulation, scores, "Nombre de simulations", "Scores")

graph(nbrSimulation, time, "Nombre de simulations", "Temps")
