Royer Quentin 
#projet L3 IA #

 - lancer le programme : `python3 main.py`  
   
 Random et Monte carlo sont jouables
 
#Questions
  - Question 4 :    
    
            Ce test permet de voir que plus le nombre d'evaluations augmente plus le temps d'execution augmente. Et il permet de voir que le l'IA
            calcule bien des coups

  - Question 14 :    
  
          il faudrait donc une fonction qui évalue chaque coup possible en leurs attribuants un "poids" en fonction de la rentabilité du coup 
          pour le joueur, et une fois tous les coups étudiés, alors on prendrait celui qui a le poids le plus rentable.
          Par exemple le fait d'entourer des zones augmenterait le poids du coup car ce type de coups permet de gagner beaucoup de points,
          ou alors de jouer des pions sur les bords du plateau permet d'entourer des zones plus rapidement donc récupérer des pions plus facilement
          Malheureusement une telle approche n'est pas envisageable sur le jeu de Go car le nombre de possibilités est beaucoup trop élevé ce
          qui demanderait une puissance de calcul énorme.
  
  - Question 15 : 
          
           Étant donné qu'Alphabeta est algorithme qui prend plus ou moins les mêmes décisions que minmax, mais qui permet d'avoir de meilleur
           temps de calcul en élégant les branches dont il pense ne pas avoir besoin.
           Ici on suppose que les temps de calculs sont instantanés.
           Alors Alphabeta a dans une centaine de parties AU PLUS 50% de chances de gagner (et donc moins s'il s'est privé des branches gagnantes
           car en élégantes certaines de ces branches il risque de se priver de certaines victoires possibles, alors que Minmax lui continue de traiter
           tous les cas possibles.
           je pense que peu importe qui commence Minmax sur le long terme gagnera plus que Alphabeta
          